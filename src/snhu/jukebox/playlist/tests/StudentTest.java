package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import snhu.jukebox.playlist.Student;
import snhu.jukebox.playlist.StudentList;
import snhu.student.playlists.*;


public class StudentTest {

	//Test the list of student names and ensure we get back the name value we expect at the correct/specific point in the list
	@Test
	public void testGetStudentNameList1() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("TestStudent1Name", studentNames.get(0));							//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
	}

	@Test
	public void testGetStudentNameList2() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("TestStudent2Name", studentNames.get(1));							//test case to see if the second value contains the name we expect
	}

	//Module 5 - Add your unit test case here to check for your name after you have added it to the StudentList
	@Test
	public void testGetStudentNameList3() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Rachel Sickler", studentNames.get(2));							//test case to see if the second value contains the name we expect
	}


	// Rylee Thompson Unit test
	@Test
	public void testGetStudentNameList4() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Rylee Thompson", studentNames.get(3));							//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
	}
	
	
	// Travis Owens Unit Test
	@Test
	public void testGetStudentNameList6() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Travis Owens", studentNames.get(5));								//test case for pass/fail. We expect the first name to be Travis Owens. Remember arrays start their count at 0 not 1.
	}
	
	// Felix Tudoran Unit Test
	@Test
	public void testGetStudentNameList7() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Felix Tudoran", studentNames.get(6));								//test case for pass/fail. We expect the first name to be Felix Tudoran. Remember arrays start their count at 0 not 1.
	}
	
	// Dan Guerin Unit Test
	@Test
	public void testGetStudentNameList8() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Dan Guerin", studentNames.get(7));								//test case for pass/fail. We expect the first name to be Dan Guerin. Remember arrays start their count at 0 not 1.
		
	}
	
	//Alex Hamilton Unit Test
	@Test
	public void testGetStudentNameListAlexHamilton() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Alex Hamilton", studentNames.get(8));
	}
	
	//Tyler Beckham Unit Test

	@Test
	public void testGetStudentNameListTylerBeckham() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Tyler Beckham", studentNames.get(9));
	}
		
	//William Seeley Unit Test
		@Test
	public void testGetStudentNameListWilliamSeeley() {
		List<String> studentNames = new ArrayList<String>();
		StudentList studentList = new StudentList();
		studentNames = studentList.getStudentsNames();
		assertEquals("William Seeley", studentNames.get(10));
	}
		
	public void testGetStudentNameListMattLee() {
		List<String> studentNames = new ArrayList<String>();
		StudentList studentList = new StudentList();
		studentNames = studentList.getStudentsNames();
		assertEquals("Matt Lee", studentNames.get(11));
	}

	
	//Andres Rodriguez Unit Test
	@Test
	public void testGetStudentNameListAndresRodriguez() {
		List<String> studentNames = new ArrayList<String>();							//Create variable for student list of names
		StudentList studentList = new StudentList();									//Instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//Populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Andres Rodriguez", studentNames.get(12));						    //Test case for pass/fail.
			
	}
	//  Igor Mozheyko Unit Test
	@Test
	public void testGetStudentNameListIgorMozheyko() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Igor Mozheyko", studentNames.get(14));							//test case for pass/fail.  Expectations that the name would be Igor Mozheyko.
	}
	
	//Module 6 Test Case Area
	//Test each student profile to ensure it can be retrieved and accessed
	@Test
	public void testGetStudent1Profile() {
		TestStudent1_Playlist testStudent1Playlist = new TestStudent1_Playlist();						//instantiating the variable for a specific student
		Student TestStudent1 = new Student("TestStudent1", testStudent1Playlist.StudentPlaylist());		//creating populated student object
		assertEquals("TestStudent1", TestStudent1.getName());											//test case pass/fail line - did the name match what you expected?
	}

	@Test
	public void testGetStudent2Profile() {
		TestStudent2_Playlist testStudent2Playlist = new TestStudent2_Playlist();
		Student TestStudent2 = new Student("TestStudent2", testStudent2Playlist.StudentPlaylist());
		assertEquals("TestStudent2", TestStudent2.getName());
	}
	@Test
	public void testGetMattLeeProfile() {
		MattLee_Playlist testMattLeePlaylist = new MattLee_Playlist();						//instantiating the variable for a specific student
		Student MattLee = new Student("MattLee", testMattLeePlaylist.StudentPlaylist());		//creating populated student object
		assertEquals("MattLee", MattLee.getName());	
	}
	@Test
	public void testGetIgorMozheykoProfile() {
		IgorMozheyko_Playlist testIgorMozheykoPlaylist = new IgorMozheyko_Playlist();						//  Instantiating the variable for a specific student
		Student IgorMozheyko = new Student("IgorMozheyko", testIgorMozheykoPlaylist.StudentPlaylist());		//  Creating populated student object
		assertEquals("IgorMozheyko", IgorMozheyko.getName());												//  Test case to pass or fail.
	}
	

	//Module 6 - Add your unit test case here to check for your profile after you have added it to the StudentList

	// Rylee Thompson Profile Unit Test
	@Test
	public void testGetRyleeThompsonProfile() {
		RyleeThompson_Playlist ryleeThompsonPlaylist = new RyleeThompson_Playlist();						//instantiating the variable for Rylee Thompson
		Student RyleeThompson = new Student("Rylee Thompson", ryleeThompsonPlaylist.StudentPlaylist());		//creating populated student object
		assertEquals("Rylee Thompson", RyleeThompson.getName());											//test case pass/fail line
	}
	
	@Test
	public void testGetTravisOwensProfile() {
		TravisOwens_Playlist testTravisOwensPlaylist = new TravisOwens_Playlist();
		Student TravisOwens = new Student("TravisOwens", testTravisOwensPlaylist.StudentPlaylist());
		assertEquals("TravisOwens", TravisOwens.getName());
	}
	
	@Test
	public void testGetAndresRodriguezProfile() {
		AndresRodriguez_Playlist testAndresRodriguezPlaylist = new AndresRodriguez_Playlist();
		Student AndresRodriguez = new Student("Andres Rodriguez", testAndresRodriguezPlaylist.StudentPlaylist());
		assertEquals("Andres Rodriguez", AndresRodriguez.getName());
	}
	
	@Test
	public void testGetNateCarlsonProfile() {
		NateCarlson_Playlist testNateCarlsonPlaylist = new NateCarlson_Playlist();
		Student NateCarlson = new Student("Nate Carlson", testNateCarlsonPlaylist.StudentPlaylist());
		assertEquals("Nate Carlson", NateCarlson.getName());
	}
}
