package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class NateCarlson_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> FortMinorTracks = new ArrayList<Song>();
    FortMinor FortMinorBand = new FortMinor();
	
    FortMinorTracks = FortMinorBand.getFortMinorSongs();
	
	playlist.add(FortMinorTracks.get(0));
	playlist.add(FortMinorTracks.get(1));
	playlist.add(FortMinorTracks.get(2));
	
	
    RobertRandolph RobertRandolphBand = new RobertRandolph();
	ArrayList<Song> RobertRandolphTracks = new ArrayList<Song>();
    RobertRandolphTracks = RobertRandolphBand.getRobertRandolphSongs();
	
	playlist.add(RobertRandolphTracks.get(0));
	playlist.add(RobertRandolphTracks.get(1));
	playlist.add(RobertRandolphTracks.get(2));
	
	
    Apache ApacheBand = new Apache();
	ArrayList<Song> ApacheTracks = new ArrayList<Song>();
	ApacheTracks = ApacheBand.getApacheSongs();
	
	playlist.add(ApacheTracks.get(0));
	playlist.add(ApacheTracks.get(1));
	playlist.add(ApacheTracks.get(2));
	
	
    Hozier HozierBand = new Hozier();
	ArrayList<Song> HozierTracks = new ArrayList<Song>();
	HozierTracks = HozierBand.getHozierSongs();
	
	playlist.add(HozierTracks.get(0));
	playlist.add(HozierTracks.get(1));
	
	
    TheProdigy TheProdigyBand = new TheProdigy();
	ArrayList<Song> TheProdigyTracks = new ArrayList<Song>();
	TheProdigyTracks = TheProdigyBand.getProdigySongs();
	
	playlist.add(TheProdigyTracks.get(0));
	playlist.add(TheProdigyTracks.get(1));
	
    return playlist;
	}
}
