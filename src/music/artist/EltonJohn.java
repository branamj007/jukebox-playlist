package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class EltonJohn {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public EltonJohn() {
    }
    
    public ArrayList<Song> getEltonJohnSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Tiny Dancer", "Elton John");         				//Create a song
         Song track2 = new Song("Your Song", "Elton John");        //Create another song
         Song track3 = new Song("Rocket Man", "Elton John");       //Create a third song
         this.albumTracks.add(track1);                                  //Add the first song to song list
         this.albumTracks.add(track2);                                  //Add the second song to song list
         this.albumTracks.add(track3);                                  //Add the third song to song list
         return albumTracks;                                            //Return the songs for Adele in the form of an ArrayList
    }
}