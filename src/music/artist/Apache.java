package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Apache {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Apache() {
    }
    
    public ArrayList<Song> getApacheSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Lacrimosa", "Apache");         				//Create a song
         Song track2 = new Song("Dies Irae", "Apache");        //Create another song
         Song track3 = new Song("Good News", "Apache");       //Create a third song
         this.albumTracks.add(track1);                                  //Add the first song to song list
         this.albumTracks.add(track2);                                  //Add the second song to song list
         this.albumTracks.add(track3);                                  //Add the third song to song list
         return albumTracks;                                            //Return the songs for Apache in the form of an ArrayList
    }
}