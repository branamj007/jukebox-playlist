package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class TylerBeckham_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		ArrayList<Song> TheStoogesSongs = new ArrayList<Song>();
	    TheStooges TheStooges = new TheStooges();
		
	    TheStoogesSongs = TheStooges.getTheStoogesSongs();
		
		playlist.add(TheStoogesSongs.get(0));
		playlist.add(TheStoogesSongs.get(1));
		playlist.add(TheStoogesSongs.get(2));
	
	
    WuTangClan WuTangClan = new WuTangClan();
	ArrayList<Song> WuTangClanSongs = new ArrayList<Song>();
	WuTangClanSongs = WuTangClan.getWuTangClanSongs();
	
	playlist.add(WuTangClanSongs.get(0));
	playlist.add(WuTangClanSongs.get(1));
	playlist.add(WuTangClanSongs.get(2));
	
    return playlist;
	}
}
