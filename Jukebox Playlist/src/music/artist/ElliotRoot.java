package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class ElliotRoot {

	ArrayList<Song> albumTracks;
    String albumTitle;

    public ElliotRoot() {
    }

    public ArrayList<Song> getElliotRootSongs() {

    	albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	Song track1 = new Song("Lost Man Running", "Elliot Root");             //Create a song
    	Song track2 = new Song("Punks and Poets", "Elliot Root");         	   //Create another song
    	this.albumTracks.add(track1);                                          //Add the first song to song list for Elliot Root
    	this.albumTracks.add(track2);                                          //Add the second song to song list for the Elliot Root
    	return albumTracks;                                                    //Return the songs for the Elliot Root in the form of an ArrayList
    }
}
